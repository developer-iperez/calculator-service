﻿using CalculatorService.Server.Domain.Journal.Dto;
using CalculatorService.Server.Domain.Operators.Dto;

namespace CalculatorService.Server.Domain.Operators.Interfaces
{
    public interface IJournalRepository
    {
        void AddJournal(JournalResult journalResult);
        QueryResult ListFilteredByTrackingId(string trackingId);
    }
}
