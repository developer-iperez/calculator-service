using CalculatorService.Server.Domain.Operators.Interfaces;
using CalculatorService.Server.Infrastructure.Journal.Memory;
using CalculatorService.Server.Services.Journal;
using CalculatorService.Server.Services.Operators;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace CalculatorService.Server
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddTransient<IAddOperator, AddOperator>();
            services.AddTransient<ISubOperator, SubOperator>();
            services.AddTransient<IMultOperator, MultOperator>();
            services.AddTransient<IDivOperator, DivOperator>();
            services.AddTransient<ISqrtOperator, SqrtOperator>();
            services.AddTransient<IOperatorJournal, OperatorJournal>();
            services.AddTransient<IQueryJournals, QueryJournals>();
            //services.AddTransient<IJournalRepository, JournalFileRepository>();
            services.AddSingleton<IJournalRepository, JournalMemoryRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
