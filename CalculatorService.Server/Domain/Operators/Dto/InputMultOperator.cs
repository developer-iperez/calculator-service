﻿using System.Collections.Generic;

namespace CalculatorService.Server.Domain.Operators.Dto
{
    public class InputMultOperator
    {
        public List<int> Factors { get; set; }
        public string TrackingId { get; set; }
    }
}
