﻿using CalculatorService.Server.Domain.Operators.Constants;
using CalculatorService.Server.Domain.Operators.Dto;
using CalculatorService.Server.Domain.Operators.Exceptions;
using CalculatorService.Server.Domain.Operators.Interfaces;
using System;

namespace CalculatorService.Server.Services.Operators
{
    public class DivOperator : IDivOperator
    {
        private readonly IOperatorJournal operatorJournal;

        public DivOperator(IOperatorJournal operatorJournal)
        {
            this.operatorJournal = operatorJournal;
        }

        public OutputDivOperator Execute(InputDivOperator input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            var operands = input.Values;
            if (operands == null || operands.Count == 0)
                throw new NoParametersException();
            if (operands.Count < 2)
                throw new AtLeastTwoOperandsRequiredException();
            if (operands[1] == 0)
                throw new DivideByZeroException();

            var resultQuotient = operands[0] / operands[1];
            var resultRemainder = operands[0] % operands[1];

            AddJournal(input, resultQuotient, resultRemainder);
            return new OutputDivOperator()
            {
                Quotient = resultQuotient,
                Remainder = resultRemainder
            };
        }

        private void AddJournal(InputDivOperator input, int quotient, int remainder)
        {
            var calculation = string.Join(" / ", input.Values);
            calculation += $" = {quotient}";
            calculation += $", remainder = {remainder}";

            operatorJournal.AddJournal(OperationTypes.Div, calculation, input.TrackingId);
        }
    }
}
