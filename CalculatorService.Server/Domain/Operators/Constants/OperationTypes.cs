﻿namespace CalculatorService.Server.Domain.Operators.Constants
{
    public class OperationTypes
    {
        public static readonly string Add = "Sum";
        public static readonly string Sub = "Sub";
        public static readonly string Mult = "Mult";
        public static readonly string Div = "Div";
        public static readonly string Sqrt = "Sqrt";
    }
}
