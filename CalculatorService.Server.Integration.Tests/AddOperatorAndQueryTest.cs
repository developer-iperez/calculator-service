using Microsoft.AspNetCore.Mvc.Testing;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Xunit;

namespace CalculatorService.Server.Integration.Tests
{
    public class AddOperatorAndQueryTest : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly WebApplicationFactory<Startup> factory;

        public AddOperatorAndQueryTest(WebApplicationFactory<Startup> factory)
        {
            this.factory = factory;
        }

        [Fact]
        public async Task Test_No_Input_Parameter_Produces_400_StatusCode()
        {
            // Arrange
            var client = factory.CreateClient();
            var serializedData = JsonSerializer.Serialize(new
            {
                Addends = ""
            });
            var data = new StringContent(serializedData, Encoding.UTF8, "application/json");

            // Act
            var response = await client.PostAsync("/calculator/add", data);
            var responseString = await response.Content.ReadAsStringAsync();

            // Assert
            Assert.NotNull(responseString);
            Assert.Equal(400, ((int)response.StatusCode));
        }

        [Fact]
        public async Task Test_Empty_Array_Input_Parameter_Produces_An_Error_Result_And_400_StatusCode()
        {
            // Arrange
            var client = factory.CreateClient();
            var serializedData = JsonSerializer.Serialize(new
            {
                Addends = new List<int>()
            });
            var data = new StringContent(serializedData, Encoding.UTF8, "application/json");

            // Act
            var response = await client.PostAsync("/calculator/add", data);
            var responseString = await response.Content.ReadAsStringAsync();

            // Assert
            Assert.NotNull(responseString);
            Assert.Equal(400, ((int)response.StatusCode));
            Assert.Contains("{\"errorCode\":\"InternalError\",\"errorStatus\":400,\"errorMessage\":\"Unable to process request: no parameters provided\"}", responseString);
        }

        [Fact]
        public async Task Test_Single_Value_Array_Input_Parameter_Produces_An_Error_Result_And_400_StatusCode()
        {
            // Arrange
            var client = factory.CreateClient();
            var serializedData = JsonSerializer.Serialize(new
            {
                Addends = new List<int>() { 1 }
            });
            var data = new StringContent(serializedData, Encoding.UTF8, "application/json");

            // Act
            var response = await client.PostAsync("/calculator/add", data);
            var responseString = await response.Content.ReadAsStringAsync();

            // Assert
            Assert.NotNull(responseString);
            Assert.Equal(400, ((int)response.StatusCode));
            Assert.Contains("{\"errorCode\":\"InternalError\",\"errorStatus\":400,\"errorMessage\":\"Unable to process request: at least two operands must be provided\"}", responseString);
        }

        [Fact]
        public async Task Test_Success_Add_Operation_Without_TrackingId_Returns_200_Status_Code_And_The_Expected_Json_Result_But_Journal_Will_Be_Empty()
        {
            // Arrange
            var trackingId = "TEST_ID";

            // We need to create a specific application context to avoid conflicting with the result of other tests
            var application = new WebApplicationFactory<Program>()
                .WithWebHostBuilder(builder =>
                {
                    // ... Configure test services
                });

            var client = application.CreateClient();
            var data = new StringContent(JsonSerializer.Serialize(new
            {
                Addends = new List<int>() { 1, 2, 3 }
            }), Encoding.UTF8, "application/json");
            var journalQueryData = new StringContent(JsonSerializer.Serialize(new
            {
                Id = trackingId
            }), Encoding.UTF8, "application/json");

            // Act
            var addResponse = await client.PostAsync("/calculator/add", data);
            var addResponseString = await addResponse.Content.ReadAsStringAsync();

            var journalResponse = await client.PostAsync("/journal/query", journalQueryData);
            var journalResponseString = await journalResponse.Content.ReadAsStringAsync();

            // Assert
            Assert.NotNull(addResponse);
            Assert.Equal(200, ((int)addResponse.StatusCode));
            Assert.Contains("{\"sum\":6}", addResponseString);

            Assert.NotNull(journalResponse);
            Assert.Equal(200, ((int)journalResponse.StatusCode));
            Assert.Contains("{\"operations\":[]}", journalResponseString);
        }

        [Fact]
        public async Task Test_Success_Add_Operation_With_TrackingId_Returns_200_Status_Code_And_The_Expected_Json_Result_And_Journal_Will_Have_The_Last_Operation()
        {
            // Arrange
            var trackingId = "TEST_ID";

            // We need to create a specific application context to avoid conflicting with the result of other tests
            var application = new WebApplicationFactory<Program>()
                .WithWebHostBuilder(builder =>
                {
                    // ... Configure test services
                });

            var client = application.CreateClient();
            client.DefaultRequestHeaders.Add("X-Evi-Tracking-Id", trackingId);

            var data = new StringContent(JsonSerializer.Serialize(new
            {
                Addends = new List<int>() { 1, 2, 3 }
            }), Encoding.UTF8, "application/json");
            var journalQueryData = new StringContent(JsonSerializer.Serialize(new
            {
                Id = "TEST_ID"
            }), Encoding.UTF8, "application/json");

            // Act
            var addResponse = await client.PostAsync("/calculator/add", data);
            var addResponseString = await addResponse.Content.ReadAsStringAsync();

            var journalResponse = await client.PostAsync("/journal/query", journalQueryData);
            var journalResponseString = await journalResponse.Content.ReadAsStringAsync();

            // Assert
            Assert.NotNull(addResponse);
            Assert.Equal(200, ((int)addResponse.StatusCode));
            Assert.Contains("{\"sum\":6}", addResponseString);

            Assert.NotNull(journalResponse);
            Assert.Equal(200, ((int)journalResponse.StatusCode));
            Assert.Contains("{\"operation\":\"Sum\",\"calculation\":\"1 \\u002B 2 \\u002B 3 = 6\"", journalResponseString);
        }
    }
}
