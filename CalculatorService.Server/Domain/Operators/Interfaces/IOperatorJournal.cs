﻿namespace CalculatorService.Server.Domain.Operators.Interfaces
{
    public interface IOperatorJournal
    {
        void AddJournal(string operatorId, string operatorExpression, string trackingId);
    }
}
