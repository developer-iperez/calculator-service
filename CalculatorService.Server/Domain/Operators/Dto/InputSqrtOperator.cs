﻿namespace CalculatorService.Server.Domain.Operators.Dto
{
    public class InputSqrtOperator
    {
        public int Value { get; set; }
        public string TrackingId { get; set; }
    }
}
