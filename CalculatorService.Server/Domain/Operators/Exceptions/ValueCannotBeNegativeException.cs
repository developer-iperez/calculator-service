﻿using System;

namespace CalculatorService.Server.Domain.Operators.Exceptions
{
    public class ValueCannotBeNegativeException : Exception
    {
        public ValueCannotBeNegativeException() : base("Value cannot be negative")
        {
        }
    }
}
