# How to build the application (server and client)

Requeriment: SDK .Net Core 3.1

Clone this project on your computer and run the batch file with the name cleanandbuild.bat

This command will execute the followings steps (all for the Release configuration):
- clean the projects
- build all projects
- publish all projects
- run tests

# How to run the server

Run the batch file with the name server.bat

# How to run the client

Run the batch file with the name client.bat. If you run the file directly, a help will be displayed.

In short, the accepted parameters are:
-o to indicate the operation to be performed (add, sub, mult, div, sqrt)
-q to request the journal for a specific trackingId
-v to pass the values to be applied with the given operation
-i to pass the (optional) value for a specific trackingId

Examples of execution:

- client -o add -v 1 2 3 -i testid
- client -o mult -v 1 2 3 -i testid
- client -o div -v 1 2 3 -i testid
- client -o sub -v 12 2 -i testid
- client -o sqrt -v 12 -i testid
- client -o q -i testid

