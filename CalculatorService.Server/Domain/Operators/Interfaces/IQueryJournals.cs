﻿using CalculatorService.Server.Domain.Journal.Dto;

namespace CalculatorService.Server.Domain.Operators.Interfaces
{
    public interface IQueryJournals
    {
        QueryResult Query(string trackingId);
    }
}
