﻿using System;

namespace CalculatorService.Server.Domain.Operators.Exceptions
{
    public class AtLeastTwoOperandsRequiredException : Exception
    {
        public AtLeastTwoOperandsRequiredException() : base("At least two operands are required")
        {
        }
    }
}
