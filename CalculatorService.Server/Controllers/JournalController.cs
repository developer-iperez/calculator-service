﻿using CalculatorService.Server.Domain.Operators.Interfaces;
using CalculatorService.Server.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace CalculatorService.Server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class JournalController : ControllerBase
    {
        private readonly IQueryJournals queryJournals;

        public JournalController(IQueryJournals queryJournals)
        {
            this.queryJournals = queryJournals;
        }

        [HttpPost]
        [Route("query")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Query([FromBody] JournalQuery journalQuery)
        {
            try
            {
                var list = queryJournals.Query(journalQuery.Id);
                return new JsonResult(list);
            }
            catch (ArgumentNullException)
            {
                return BadRequest(new
                {
                    ErrorCode = "InternalError",
                    ErrorStatus = 400,
                    ErrorMessage = "Unable to process request: invalid argument provided"
                });
            }
            catch (Exception)
            {
                Response.StatusCode = StatusCodes.Status500InternalServerError;
                return new JsonResult(new
                {
                    ErrorCode = "InternalError",
                    ErrorStatus = 500,
                    ErrorMessage = "An unexpected error condition was triggered which made impossible to fulfill the request. Please try again or contact support."
                });
            }
        }
    }
}
