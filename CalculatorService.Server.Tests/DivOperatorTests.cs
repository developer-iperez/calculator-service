﻿using CalculatorService.Server.Domain.Operators.Dto;
using CalculatorService.Server.Domain.Operators.Exceptions;
using CalculatorService.Server.Domain.Operators.Interfaces;
using CalculatorService.Server.Services.Operators;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace CalculatorService.Server.Tests
{
    public class DivOperatorTests
    {
        private readonly Mock<IOperatorJournal> operatorJournalMock;

        public DivOperatorTests()
        {
            operatorJournalMock = new Mock<IOperatorJournal>();
        }

        [Fact]
        public void Test_No_Input_Parameter_Throws_ArgumentNullException()
        {
            // Arrange
            var service = new DivOperator(operatorJournalMock.Object);

            // Act
            // Assert
            var exception = Assert.Throws<ArgumentNullException>(() =>
            {
                service.Execute(null);
            });

            operatorJournalMock.Verify(x => x.AddJournal(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            Assert.Equal("Value cannot be null. (Parameter 'input')", exception.Message);
        }

        [Fact]
        public void Test_No_Input_Values_Throws_NoInputParametersException()
        {
            // Arrange
            var service = new DivOperator(operatorJournalMock.Object);

            // Act
            // Assert
            var exception = Assert.Throws<NoParametersException>(() =>
            {
                service.Execute(new InputDivOperator()
                {
                    Values = null
                });
            });

            operatorJournalMock.Verify(x => x.AddJournal(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            Assert.Equal("No parameteres provided", exception.Message);
        }

        [Fact]
        public void Test_Input_Parameters_As_Empty_Array_Throws_NoInputParametersException()
        {
            // Arrange
            var service = new DivOperator(operatorJournalMock.Object);

            // Act
            // Assert
            var exception = Assert.Throws<NoParametersException>(() =>
            {
                service.Execute(new InputDivOperator()
                {
                    Values = new List<int>()
                });
            });

            operatorJournalMock.Verify(x => x.AddJournal(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            Assert.Equal("No parameteres provided", exception.Message);
        }

        [Fact]
        public void Test_One_Parameter_Provided_Throws_AtLeastTwoOperandsRequiredException()
        {
            // Arrange
            var service = new DivOperator(operatorJournalMock.Object);

            // Act
            // Assert
            var exception = Assert.Throws<AtLeastTwoOperandsRequiredException>(() =>
            {
                service.Execute(new InputDivOperator()
                {
                    Values = new List<int>() { 1 }
                });
            });

            operatorJournalMock.Verify(x => x.AddJournal(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            Assert.Equal("At least two operands are required", exception.Message);
        }

        [Fact]
        public void Test_Two_Operands_As_Input_Returns_The_Total_Added_Expected_Value()
        {
            // Arrange
            var service = new DivOperator(operatorJournalMock.Object);

            // Act
            var result = service.Execute(new InputDivOperator()
            {
                Values = new List<int>() { 125, 21 }
            });

            // Assert
            operatorJournalMock.Verify(x => x.AddJournal(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            Assert.Equal(5, result.Quotient);
            Assert.Equal(20, result.Remainder);
        }

        [Fact]
        public void Test_Division_By_Zero()
        {
            // Arrange
            var service = new DivOperator(operatorJournalMock.Object);

            // Act
            // Assert
            var exception = Assert.Throws<DivideByZeroException>(() =>
            {
                service.Execute(new InputDivOperator()
                {
                    Values = new List<int>() { 10, 0 }
                });
            });

            operatorJournalMock.Verify(x => x.AddJournal(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            Assert.Equal("Attempted to divide by zero.", exception.Message);
        }
    }
}
