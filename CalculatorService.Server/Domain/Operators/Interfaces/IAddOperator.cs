﻿using CalculatorService.Server.Domain.Operators.Dto;

namespace CalculatorService.Server.Domain.Operators.Interfaces
{
    public interface IAddOperator
    {
        int Execute(InputAddOperator input);
    }
}
