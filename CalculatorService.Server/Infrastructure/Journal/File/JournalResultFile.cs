﻿using FileHelpers;

namespace CalculatorService.Server.Infrastructure.Journal.File
{
    [DelimitedRecord(",")]
    public class JournalResultFile
    {
        public string RequestId { get; set; }
        public string Operation { get; set; }
        public string Calculation { get; set; }
        public string Date { get; set; }
    }
}
