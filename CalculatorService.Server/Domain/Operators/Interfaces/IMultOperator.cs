﻿using CalculatorService.Server.Domain.Operators.Dto;

namespace CalculatorService.Server.Domain.Operators.Interfaces
{
    public interface IMultOperator
    {
        int Execute(InputMultOperator input);
    }
}
