﻿using CalculatorService.Server.Domain.Operators.Constants;
using CalculatorService.Server.Domain.Operators.Dto;
using CalculatorService.Server.Domain.Operators.Exceptions;
using CalculatorService.Server.Domain.Operators.Interfaces;
using System;

namespace CalculatorService.Server.Services.Operators
{
    public class SubOperator : ISubOperator
    {
        private readonly IOperatorJournal operatorJournal;

        public SubOperator(IOperatorJournal operatorJournal)
        {
            this.operatorJournal = operatorJournal;
        }

        public int Execute(InputSubOperator input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            var operands = input.Values;
            if (operands == null || operands.Count == 0)
                throw new NoParametersException();
            if (operands.Count < 2)
                throw new AtLeastTwoOperandsRequiredException();

            var result = 0;

            for (var i = 0; i < operands.Count; i++)
            {
                if (i == 0)
                    result = operands[i];
                else
                    result -= operands[i];
            }

            AddJournal(input, result);
            return result;
        }

        private void AddJournal(InputSubOperator input, int result)
        {
            var calculation = string.Join(" - ", input.Values);
            calculation += " = ";
            calculation += result;

            operatorJournal.AddJournal(OperationTypes.Sub, calculation, input.TrackingId);
        }
    }
}
