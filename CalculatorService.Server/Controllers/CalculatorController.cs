﻿using CalculatorService.Server.Domain.Operators.Dto;
using CalculatorService.Server.Domain.Operators.Exceptions;
using CalculatorService.Server.Domain.Operators.Interfaces;
using CalculatorService.Server.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;

namespace CalculatorService.Server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CalculatorController : ControllerBase
    {
        private readonly IAddOperator addOperator;
        private readonly ISubOperator subOperator;
        private readonly IMultOperator multOperator;
        private readonly IDivOperator divOperator;
        private readonly ISqrtOperator sqrtOperator;

        public CalculatorController(IAddOperator addOperator, ISubOperator subOperator, IMultOperator multOperator,
            IDivOperator divOperator, ISqrtOperator sqrtOperator)
        {
            this.addOperator = addOperator;
            this.subOperator = subOperator;
            this.multOperator = multOperator;
            this.divOperator = divOperator;
            this.sqrtOperator = sqrtOperator;
        }

        [HttpPost]
        [Route("add")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Add([FromBody] AddOperatorCommand addOperatorCommand)
        {
            try
            {
                string xEviTrackingId = null;
                var stringValues = new StringValues();

                if (Request.Headers.TryGetValue("X-Evi-Tracking-Id", out stringValues))
                    xEviTrackingId = stringValues[0];

                var result = addOperator.Execute(new InputAddOperator()
                {
                    TrackingId = xEviTrackingId,
                    Values = new List<int>(addOperatorCommand.Addends)
                });

                return new JsonResult(new
                {
                    Sum = result
                });
            }
            catch (NoParametersException)
            {
                return BadRequest(new
                {
                    ErrorCode = "InternalError",
                    ErrorStatus = 400,
                    ErrorMessage = "Unable to process request: no parameters provided"
                });
            }
            catch (AtLeastTwoOperandsRequiredException)
            {
                return BadRequest(new
                {
                    ErrorCode = "InternalError",
                    ErrorStatus = 400,
                    ErrorMessage = "Unable to process request: at least two operands must be provided"
                });
            }
            catch (Exception)
            {
                Response.StatusCode = StatusCodes.Status500InternalServerError;
                return new JsonResult(new
                {
                    ErrorCode = "InternalError",
                    ErrorStatus = 500,
                    ErrorMessage = "An unexpected error condition was triggered which made impossible to fulfill the request. Please try again or contact support."
                });
            }
        }

        [HttpPost]
        [Route("sub")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Sub([FromBody] SubOperatorCommand subOperatorCommand)
        {
            try
            {
                string xEviTrackingId = null;
                var stringValues = new StringValues();

                if (Request.Headers.TryGetValue("X-Evi-Tracking-Id", out stringValues))
                    xEviTrackingId = stringValues[0];

                var result = subOperator.Execute(new InputSubOperator()
                {
                    TrackingId = xEviTrackingId,
                    Values = new List<int>()
                    {
                        subOperatorCommand.Minuend,
                        subOperatorCommand.Subtrahend
                    }
                });

                return new JsonResult(new
                {
                    Difference = result
                });
            }
            catch (NoParametersException)
            {
                return BadRequest(new
                {
                    ErrorCode = "InternalError",
                    ErrorStatus = 400,
                    ErrorMessage = "Unable to process request: no parameters provided"
                });
            }
            catch (AtLeastTwoOperandsRequiredException)
            {
                return BadRequest(new
                {
                    ErrorCode = "InternalError",
                    ErrorStatus = 400,
                    ErrorMessage = "Unable to process request: at least two operands must be provided"
                });
            }
            catch (Exception)
            {
                Response.StatusCode = StatusCodes.Status500InternalServerError;
                return new JsonResult(new
                {
                    ErrorCode = "InternalError",
                    ErrorStatus = 500,
                    ErrorMessage = "An unexpected error condition was triggered which made impossible to fulfill the request. Please try again or contact support."
                });
            }
        }

        [HttpPost]
        [Route("mult")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Mult([FromBody] MultOperatorCommand multOperatorCommand)
        {
            try
            {
                string xEviTrackingId = null;
                var stringValues = new StringValues();

                if (Request.Headers.TryGetValue("X-Evi-Tracking-Id", out stringValues))
                    xEviTrackingId = stringValues[0];

                var result = multOperator.Execute(new InputMultOperator()
                {
                    TrackingId = xEviTrackingId,
                    Factors = new List<int>(multOperatorCommand.Factors)
                });

                return new JsonResult(new
                {
                    Product = result
                });
            }
            catch (NoParametersException)
            {
                return BadRequest(new
                {
                    ErrorCode = "InternalError",
                    ErrorStatus = 400,
                    ErrorMessage = "Unable to process request: no parameters provided"
                });
            }
            catch (AtLeastTwoOperandsRequiredException)
            {
                return BadRequest(new
                {
                    ErrorCode = "InternalError",
                    ErrorStatus = 400,
                    ErrorMessage = "Unable to process request: at least two operands must be provided"
                });
            }
            catch (Exception)
            {
                Response.StatusCode = StatusCodes.Status500InternalServerError;
                return new JsonResult(new
                {
                    ErrorCode = "InternalError",
                    ErrorStatus = 500,
                    ErrorMessage = "An unexpected error condition was triggered which made impossible to fulfill the request. Please try again or contact support."
                });
            }
        }

        [HttpPost]
        [Route("div")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Div([FromBody] DivOperatorCommand divOperatorCommand)
        {
            try
            {
                string xEviTrackingId = null;
                var stringValues = new StringValues();

                if (Request.Headers.TryGetValue("X-Evi-Tracking-Id", out stringValues))
                    xEviTrackingId = stringValues[0];

                var result = divOperator.Execute(new InputDivOperator()
                {
                    TrackingId = xEviTrackingId,
                    Values = new List<int>()
                    {
                        divOperatorCommand.Dividend,
                        divOperatorCommand.Divisor
                    }
                });

                return new JsonResult(new
                {
                    Quotient = result.Quotient,
                    Remainder = result.Remainder
                });
            }
            catch (NoParametersException)
            {
                return BadRequest(new
                {
                    ErrorCode = "InternalError",
                    ErrorStatus = 400,
                    ErrorMessage = "Unable to process request: no parameters provided"
                });
            }
            catch (AtLeastTwoOperandsRequiredException)
            {
                return BadRequest(new
                {
                    ErrorCode = "InternalError",
                    ErrorStatus = 400,
                    ErrorMessage = "Unable to process request: at least two operands must be provided"
                });
            }
            catch (Exception)
            {
                Response.StatusCode = StatusCodes.Status500InternalServerError;
                return new JsonResult(new
                {
                    ErrorCode = "InternalError",
                    ErrorStatus = 500,
                    ErrorMessage = "An unexpected error condition was triggered which made impossible to fulfill the request. Please try again or contact support."
                });
            }
        }

        [HttpPost]
        [Route("sqrt")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Sqrt([FromBody] SqrtOperatorCommand divOperatorCommand)
        {
            try
            {
                string xEviTrackingId = null;
                var stringValues = new StringValues();

                if (Request.Headers.TryGetValue("X-Evi-Tracking-Id", out stringValues))
                    xEviTrackingId = stringValues[0];

                var result = sqrtOperator.Execute(new InputSqrtOperator()
                {
                    TrackingId = xEviTrackingId,
                    Value = divOperatorCommand.Number
                });

                return new JsonResult(new
                {
                    Square = result
                });
            }
            catch (NoParametersException)
            {
                return BadRequest(new
                {
                    ErrorCode = "InternalError",
                    ErrorStatus = 400,
                    ErrorMessage = "Unable to process request: no parameters provided"
                });
            }
            catch (AtLeastTwoOperandsRequiredException)
            {
                return BadRequest(new
                {
                    ErrorCode = "InternalError",
                    ErrorStatus = 400,
                    ErrorMessage = "Unable to process request: at least two operands must be provided"
                });
            }
            catch (Exception)
            {
                Response.StatusCode = StatusCodes.Status500InternalServerError;
                return new JsonResult(new
                {
                    ErrorCode = "InternalError",
                    ErrorStatus = 500,
                    ErrorMessage = "An unexpected error condition was triggered which made impossible to fulfill the request. Please try again or contact support."
                });
            }
        }
    }
}
