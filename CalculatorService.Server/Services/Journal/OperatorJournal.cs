﻿using CalculatorService.Server.Domain.Operators.Dto;
using CalculatorService.Server.Domain.Operators.Interfaces;
using System;

namespace CalculatorService.Server.Services.Journal
{
    public class OperatorJournal : IOperatorJournal
    {
        private readonly IJournalRepository journalRepository;

        public OperatorJournal(IJournalRepository journalRepository)
        {
            this.journalRepository = journalRepository;
        }

        public void AddJournal(string operatorId, string operatorExpression, string trackingId)
        {
            if (trackingId == null || string.IsNullOrEmpty(trackingId))
                return;

            journalRepository.AddJournal(new JournalResult()
            {
                Calculation = operatorExpression,
                Date = DateTime.UtcNow,
                Operation = operatorId,
                RequestId = trackingId
            });
        }
    }
}
