﻿namespace CalculatorService.Server.Model
{
    public class AddOperatorCommand
    {
        public int[] Addends { get; set; }
    }
}
