﻿using System;

namespace CalculatorService.Server.Domain.Operators.Exceptions
{
    public class NoParametersException : Exception
    {
        public NoParametersException() : base("No parameteres provided")
        {
        }
    }
}
