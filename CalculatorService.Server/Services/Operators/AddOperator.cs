﻿using CalculatorService.Server.Domain.Operators.Constants;
using CalculatorService.Server.Domain.Operators.Dto;
using CalculatorService.Server.Domain.Operators.Exceptions;
using CalculatorService.Server.Domain.Operators.Interfaces;
using System;

namespace CalculatorService.Server.Services.Operators
{
    public class AddOperator : IAddOperator
    {
        private readonly IOperatorJournal operatorJournal;

        public AddOperator(IOperatorJournal operatorJournal)
        {
            this.operatorJournal = operatorJournal;
        }

        public int Execute(InputAddOperator input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            var operands = input.Values;
            if (operands == null || operands.Count == 0)
                throw new NoParametersException();
            if (operands.Count < 2)
                throw new AtLeastTwoOperandsRequiredException();

            var result = 0;           
            operands.ForEach(x => result += x);

            AddJournal(input, result);
            return result;
        }

        private void AddJournal(InputAddOperator input, int result)
        {
            var calculation = string.Join(" + ", input.Values);
            calculation += " = ";
            calculation += result;

            operatorJournal.AddJournal(OperationTypes.Add, calculation, input.TrackingId);
        }
    }
}
