﻿namespace CalculatorService.Server.Model
{
    public class DivOperatorCommand
    {
        public int Dividend { get; set; }
        public int Divisor { get; set; }
    }
}
