﻿using CalculatorService.Server.Domain.Operators.Constants;
using CalculatorService.Server.Domain.Operators.Dto;
using CalculatorService.Server.Domain.Operators.Exceptions;
using CalculatorService.Server.Domain.Operators.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalculatorService.Server.Services.Operators
{
    public class SqrtOperator : ISqrtOperator
    {
        private readonly IOperatorJournal operatorJournal;

        public SqrtOperator(IOperatorJournal operatorJournal)
        {
            this.operatorJournal = operatorJournal;
        }

        public double Execute(InputSqrtOperator input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));
            if (input.Value < 0)
                throw new ValueCannotBeNegativeException();

            var result = Math.Sqrt(input.Value);

            AddJournal(input, result);
            return result;
        }

        private void AddJournal(InputSqrtOperator input, double result)
        {
            var calculation = $"sqrt({input.Value}) = {result}";

            operatorJournal.AddJournal(OperationTypes.Sqrt, calculation, input.TrackingId);
        }
    }
}
