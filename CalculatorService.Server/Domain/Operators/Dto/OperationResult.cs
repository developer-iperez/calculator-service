﻿namespace CalculatorService.Server.Domain.Operators.Dto
{
    public class OperationResult
    {
        public string Operator { get; set; }
        public string Operands { get; set; }
        public int Result { get; set; }
    }
}
