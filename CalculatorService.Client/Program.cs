﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace CalculatorService.Client
{

    public class Options
    {
        [Option('o', "operation", Required = true, SetName = "Operations", HelpText = "Operation to be performed. Valid operations: add, sub, mult, div, sqrt")]
        public string Operation { get; set; }

        [Option('q', "query", Required = true, SetName = "Query", HelpText = "Query the stored results indexed by the parameter 'identifier'")]
        public bool Query { get; set; }

        [Option('v', "values", Required = true, SetName = "Operations", HelpText = "Values to be applied to the operation")]
        public IEnumerable<int> Values { get; set; }

        [Option('i', "identifier", Required = false, HelpText = "'X­Evi­Tracking­Id’ header, required only with the parameter 'query'")]
        public string RequestIdentifier { get; set; }
    }

    public class OperationRequestClient
    {
        public OperationRequestClient()
        {
                       
        }

        public async Task Execute(string operation, IEnumerable<int> values, string requestIdentifier)
        {
            var url = "http://127.0.0.1:5000/calculator";

            var httpClient = new HttpClient();
            httpClient.Timeout = new TimeSpan(0, 0, 5);

            if (requestIdentifier != null)
                httpClient.DefaultRequestHeaders.Add("X-Evi-Tracking-Id", requestIdentifier);

            string serializedData = null;
            
            switch(operation)
            {
                case "add":
                    url += "/add";
                    serializedData = JsonSerializer.Serialize(new
                    {
                        Addends = values
                    });
                    break;

                case "sub":
                    if (values.Count() == 2)
                    {
                        url += "/sub";
                        serializedData = JsonSerializer.Serialize(new
                        {
                            Minuend = values.ElementAt(0),
                            Subtrahend = values.ElementAt(1)
                        });
                    }
                    break;

                case "mult":
                    url += "/mult";
                    serializedData = JsonSerializer.Serialize(new
                    {
                        Factors = values
                    });
                    break;

                case "div":
                    if (values.Count() == 2)
                    {
                        url += "/div";
                        serializedData = JsonSerializer.Serialize(new
                        {
                            Dividend = values.ElementAt(0),
                            Divisor = values.ElementAt(1)
                        });
                    }
                    break;

                case "sqrt":
                    if (values.Count() == 1)
                    {
                        url += "/sqrt";
                        serializedData = JsonSerializer.Serialize(new
                        {
                            Number = values.ElementAt(0)
                        });
                    }
                    break;

                default:
                    break;
            }

            if (serializedData == null)
                return;

            var data = new StringContent(serializedData, Encoding.UTF8, "application/json");
            var response = await httpClient.PostAsync(url, data);
            var result = await response.Content.ReadAsStringAsync();

            Console.WriteLine($"{(int) response.StatusCode} {response.StatusCode}");
            Console.WriteLine($"Content-Type: { response.Content.Headers.ContentType }");
            Console.WriteLine($"Content-Length: { response.Content.Headers.ContentLength }");
            Console.WriteLine($"\n{result}");
        }
    }

    public class QueryRequestClient
    {
        public QueryRequestClient()
        {

        }

        public async Task Execute(string requestIdentifier)
        {
            var url = "http://127.0.0.1:5000/journal/query";

            var httpClient = new HttpClient();
            httpClient.Timeout = new TimeSpan(0, 0, 5);

            var serializedData = JsonSerializer.Serialize(new
            {
                Id = requestIdentifier
            });
            var data = new StringContent(serializedData, Encoding.UTF8, "application/json");
            var response = await httpClient.PostAsync(url, data);
            var result = await response.Content.ReadAsStringAsync();

            Console.WriteLine($"{(int)response.StatusCode} {response.StatusCode}");
            Console.WriteLine($"Content-Type: { response.Content.Headers.ContentType }");
            Console.WriteLine($"Content-Length: { response.Content.Headers.ContentLength }");
            Console.WriteLine($"\n{result}");
        }
    }

    /// <summary>
    /// A demonstration console/command­line client, capable of performing requests to the main HTTP service from CLI.
    /// </summary>
    class Program
    {
        static async Task Main(string[] args)
        {
            string operation = null;
            IEnumerable<int> values = null;
            string requestIdentifier = null;
            bool query = false;

            var parser = new Parser(with =>
            {
                with.HelpWriter = Console.Error;
                with.EnableDashDash = true;
                with.IgnoreUnknownArguments = true;
            });

            var parserResult = parser.ParseArguments<Options>(args);
            parserResult
                .WithParsed<Options>(o =>
                {
                    operation = o.Operation;
                    values = o.Values;
                    requestIdentifier = o.RequestIdentifier;
                    query = o.Query;
                })
                .WithNotParsed<Options>(errors =>
                {
                    foreach(var error in errors)
                    {
                        Console.WriteLine(error.ToString());
                    }
                });

            if ((operation == null || values == null) && query == false)
                return;

            try
            {
                if (query)
                {
                    var requestHelper = new QueryRequestClient();
                    await requestHelper.Execute(requestIdentifier);
                }
                else
                {
                    var requestHelper = new OperationRequestClient();
                    await requestHelper.Execute(operation, values, requestIdentifier);
                }
            }
            catch (HttpRequestException exception)
            {
                Console.WriteLine($"Exception was thrown: {exception.Message}");
            }
            catch (TaskCanceledException)
            {
                Console.WriteLine($"The current request exceeded the maximum time required");
            }
        }
    }
}
