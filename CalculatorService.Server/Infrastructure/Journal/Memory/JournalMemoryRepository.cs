﻿using CalculatorService.Server.Domain.Journal.Dto;
using CalculatorService.Server.Domain.Operators.Dto;
using CalculatorService.Server.Domain.Operators.Interfaces;
using System.Collections.Generic;
using System.Threading;

namespace CalculatorService.Server.Infrastructure.Journal.Memory
{
    public class JournalMemoryRepository : IJournalRepository
    {
        private readonly Dictionary<string, List<JournalResult>> store;

        private readonly int DefaultMonitorTimeoutInMilliseconds = 5000;

        public JournalMemoryRepository()
        {
            store = new Dictionary<string, List<JournalResult>>();
        }

        public void AddJournal(JournalResult journalResult)
        {
            if (journalResult == null)
                return;

            try
            {
                Monitor.TryEnter(store, DefaultMonitorTimeoutInMilliseconds);

                List<JournalResult> list;

                if (!store.ContainsKey(journalResult.RequestId))
                    store.TryAdd(journalResult.RequestId, new List<JournalResult>());

                if (store.TryGetValue(journalResult.RequestId, out list))
                    list.Add(journalResult);
            }
            finally
            {
                Monitor.Exit(store);
            }
        }

        public QueryResult ListFilteredByTrackingId(string trackingId)
        {
            var result = new QueryResult();
            List<JournalResult> listFromStore;

            result.Operations = new List<QueryItemResult>();

            try
            {
                Monitor.TryEnter(store, DefaultMonitorTimeoutInMilliseconds);

                store.TryGetValue(trackingId, out listFromStore);
            }
            finally
            {
                Monitor.Exit(store);
            }

            if (listFromStore != null)
            {
                foreach (JournalResult journal in listFromStore)
                {
                    if (journal.RequestId != trackingId)
                        continue;

                    result.Operations.Add(new QueryItemResult()
                    {
                        Calculation = journal.Calculation,
                        Date = journal.Date,
                        Operation = journal.Operation
                    });
                }
            }

            return result;
        }
    }
}
