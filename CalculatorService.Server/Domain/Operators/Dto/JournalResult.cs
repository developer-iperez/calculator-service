﻿using System;

namespace CalculatorService.Server.Domain.Operators.Dto
{
    public class JournalResult
    {
        public string RequestId { get; set; }
        public string Operation { get; set; }
        public string Calculation { get; set; }
        public DateTime Date { get; set; }
    }
}
