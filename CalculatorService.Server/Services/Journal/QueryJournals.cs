﻿using CalculatorService.Server.Domain.Journal.Dto;
using CalculatorService.Server.Domain.Operators.Dto;
using CalculatorService.Server.Domain.Operators.Interfaces;
using System;
using System.Collections.Generic;

namespace CalculatorService.Server.Services.Journal
{
    public class QueryJournals : IQueryJournals
    {
        private readonly IJournalRepository journalRepository;

        public QueryJournals(IJournalRepository journalRepository)
        {
            this.journalRepository = journalRepository;
        }

        public QueryResult Query(string trackingId)
        {
            if (trackingId == null)
                throw new ArgumentNullException(nameof(trackingId));

            var list = journalRepository.ListFilteredByTrackingId(trackingId);
            return list;
        }
    }
}
