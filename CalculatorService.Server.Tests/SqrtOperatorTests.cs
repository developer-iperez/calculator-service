﻿using CalculatorService.Server.Domain.Operators.Dto;
using CalculatorService.Server.Domain.Operators.Exceptions;
using CalculatorService.Server.Domain.Operators.Interfaces;
using CalculatorService.Server.Services.Operators;
using Moq;
using System;
using Xunit;

namespace CalculatorService.Server.Tests
{
    public class SqrtOPeratorTests
    {
        private readonly Mock<IOperatorJournal> operatorJournalMock;

        public SqrtOPeratorTests()
        {
            operatorJournalMock = new Mock<IOperatorJournal>();
        }

        [Fact]
        public void Test_No_Input_Parameter_Throws_ArgumentNullException()
        {
            // Arrange
            var service = new SqrtOperator(operatorJournalMock.Object);

            // Act
            // Assert
            var exception = Assert.Throws<ArgumentNullException>(() =>
            {
                service.Execute(null);
            });

            operatorJournalMock.Verify(x => x.AddJournal(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            Assert.Equal("Value cannot be null. (Parameter 'input')", exception.Message);
        }

        [Fact]
        public void Test_One_Operand_As_Input_Returns_The_Calculated_Square_Result()
        {
            // Arrange
            var service = new SqrtOperator(operatorJournalMock.Object);

            // Act
            var result = service.Execute(new InputSqrtOperator()
            {
                Value = 64
            });

            // Assert
            operatorJournalMock.Verify(x => x.AddJournal(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            Assert.Equal(8, result);
        }

        [Fact]
        public void Test_Input_Parameter_As_Negative_Number_Throws_ValueCannotBeNegativeException()
        {
            // Arrange
            var service = new SqrtOperator(operatorJournalMock.Object);

            // Act
            // Assert
            var exception = Assert.Throws<ValueCannotBeNegativeException>(() =>
            {
                service.Execute(new InputSqrtOperator()
                {
                    Value = -1
                });
            });

            operatorJournalMock.Verify(x => x.AddJournal(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            Assert.Equal("Value cannot be negative", exception.Message);
        }
    }
}
