﻿using CalculatorService.Server.Domain.Operators.Dto;

namespace CalculatorService.Server.Domain.Operators.Interfaces
{
    public interface ISqrtOperator
    {
        double Execute(InputSqrtOperator input);
    }
}
