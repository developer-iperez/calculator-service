﻿using CalculatorService.Server.Domain.Operators.Dto;

namespace CalculatorService.Server.Domain.Operators.Interfaces
{
    public interface ISubOperator
    {
        int Execute(InputSubOperator input);
    }
}
