﻿namespace CalculatorService.Server.Domain.Operators.Dto
{
    public class OutputDivOperator
    {
        public int Quotient { get; set; }
        public int Remainder { get; set; }
    }
}
