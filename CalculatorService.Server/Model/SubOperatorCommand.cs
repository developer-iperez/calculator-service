﻿namespace CalculatorService.Server.Model
{
    public class SubOperatorCommand
    {
        public int Minuend { get; set; }
        public int Subtrahend { get; set; }
    }
}
