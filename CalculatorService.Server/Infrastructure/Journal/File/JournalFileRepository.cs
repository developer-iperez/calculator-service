﻿using CalculatorService.Server.Domain.Journal.Dto;
using CalculatorService.Server.Domain.Operators.Dto;
using CalculatorService.Server.Domain.Operators.Interfaces;
using FileHelpers;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace CalculatorService.Server.Infrastructure.Journal.File
{
    public class JournalFileRepository : IJournalRepository
    {
        private readonly FileHelperAsyncEngine<JournalResultFile> fileHelperJournals;
        private readonly IConfiguration configuration;

        private readonly int DefaultMonitorTimeoutInMilliseconds = 5000;

        public JournalFileRepository(IConfiguration configuration)
        {
            this.configuration = configuration;
            fileHelperJournals = new FileHelperAsyncEngine<JournalResultFile>();
        }

        public void AddJournal(JournalResult journalResult)
        {
            try
            {
                Monitor.TryEnter(fileHelperJournals, DefaultMonitorTimeoutInMilliseconds);

                using (fileHelperJournals.BeginAppendToFile(configuration["JournalsFile"]))
                {
                    fileHelperJournals.WriteNext(new JournalResultFile()
                    {
                        Calculation = journalResult.Calculation,
                        Date = journalResult.Date.ToString("s"),
                        Operation = journalResult.Operation,
                        RequestId = journalResult.RequestId
                    });
                }
            }
            finally
            {
                Monitor.Exit(fileHelperJournals);
            }
        }

        public QueryResult ListFilteredByTrackingId(string trackingId)
        {
            var result = new QueryResult();
            List<JournalResultFile> listFromFile = null;

            result.Operations = new List<QueryItemResult>();

            try
            {
                Monitor.TryEnter(fileHelperJournals, DefaultMonitorTimeoutInMilliseconds);

                using (fileHelperJournals.BeginReadFile(configuration["JournalsFile"]))
                {
                    listFromFile = fileHelperJournals.ToList();
                }
            }
            finally
            {
                Monitor.Exit(fileHelperJournals);
            }

            if (listFromFile != null)
            {
                foreach (JournalResultFile journal in listFromFile)
                {
                    if (journal.RequestId != trackingId)
                        continue;

                    result.Operations.Add(new QueryItemResult()
                    {
                        Calculation = journal.Calculation,
                        Date = DateTime.Parse(journal.Date),
                        Operation = journal.Operation
                    });
                }
            }

            return result;
        }
    }
}
