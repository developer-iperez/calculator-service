﻿using System.Collections.Generic;

namespace CalculatorService.Server.Domain.Journal.Dto
{
    public class QueryResult
    {
        public List<QueryItemResult> Operations { get; set; }
    }
}
