﻿using System.Collections.Generic;

namespace CalculatorService.Server.Domain.Operators.Dto
{
    public class InputAddOperator
    {
        public List<int> Values { get; set; }
        public string TrackingId { get; set; }
    }
}
