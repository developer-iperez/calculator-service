﻿using System;

namespace CalculatorService.Server.Domain.Journal.Dto
{
    public class QueryItemResult
    {
        public string Operation { get; set; }
        public string Calculation { get; set; }
        public DateTime Date { get; set; }
    }
}
