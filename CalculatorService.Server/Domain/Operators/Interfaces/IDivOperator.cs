﻿using CalculatorService.Server.Domain.Operators.Dto;

namespace CalculatorService.Server.Domain.Operators.Interfaces
{
    public interface IDivOperator
    {
        OutputDivOperator Execute(InputDivOperator input);
    }
}
